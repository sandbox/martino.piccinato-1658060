<?php

/**
 * @file
 * Plugin to provide an relationship handler for all terms from user (blatantly copied from terms_from_node.inc)
 */

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
    'title' => t('Multiple terms from user'),
    'keyword' => 'terms',
    'description' => t('Adds a taxonomy terms from a user context; if multiple terms are selected, they wil be concatenated.'),
    'required context' => new ctools_context_required(t('User'), 'user'),
    'context' => 'ctools_entitiesfromfields_context',
    'edit form' => 'ctools_entitiesfromfields_settings_form',
    'defaults' => array('vocabulary' => array(), 'term_field' => array(), 'concatenator' => ','),
);

/**
 * Return a new context based on an existing context.
 */
function ctools_entitiesfromfields_context($context, $conf)
{
    // If unset it wants a generic, unfilled context, which is just NULL.
    if (empty($context->data)) {
        return ctools_context_create_empty('terms', NULL);
    }

    // Collect all terms for the chosen vocabulary and concatenate them.
    $user = $context->data;
    $terms = array();

    $fields = field_info_instances('user', 'user');

    foreach ($fields as $name => $info) {
        $field_info = field_info_field($name);
        if ($field_info['type'] == 'taxonomy_term_reference'
            && (empty($conf['term_field']) || $conf['term_field'][$name])
            && (empty($conf['vocabulary']) || $conf['vocabulary'][$field_info['settings']['allowed_values'][0]['vocabulary']])
        ) {
            $items = field_get_items('user', $user, $name);
            if (is_array($items)) {
                foreach ($items as $item) {
                    $terms[] = $item['tid'];
                }
            }
        }
    }

    if (!empty($terms)) {
        $all_terms = ctools_break_phrase(implode($conf['concatenator'], $terms));
        return ctools_context_create('terms', $all_terms);
    }

    // No term
    return ctools_context_create('terms', '');
}

/**
 * Settings form for the relationship.
 */
function ctools_entitiesfromfields_settings_form($form, &$form_state)
{
    $conf = $form_state['conf'];

    $options_voc = array();
    foreach (taxonomy_vocabulary_get_names() as $name => $vocabulary) {
        $options_voc[$name] = $vocabulary->name;
    }

    $fields = field_info_instances('user', 'user');
    /*
    print "<pre>";
    print_r($fields);
    print "</pre>";
    exit(0);
    */

    $options_field = array();
    foreach ($fields as $name => $field) {
        $field_info = field_info_field($name);
        if ($field_info['type'] == 'taxonomy_term_reference') {
            $options_field[$name] = $field['label'];
        }
    }

    $form['term_field'] = array(
        '#title' => t('Field'),
        '#type' => 'checkboxes',
        '#options' => $options_field,
        '#default_value' => $conf['term_field'],
        '#prefix' => '<div class="clearfix">',
        '#suffix' => '</div>',
        '#description' => t('Limit the terms to those of a specific user field (no selection will mean terms from all fields)')
    );

    $form['vocabulary'] = array(
        '#title' => t('Vocabulary'),
        '#type' => 'checkboxes',
        '#options' => $options_voc,
        '#default_value' => $conf['vocabulary'],
        '#prefix' => '<div class="clearfix">',
        '#suffix' => '</div>',
        '#description' => t('Limit the terms to those belonging to the selected vocabularies (no selection will mean terms from any vocabulary)')
    );

    $form['concatenator'] = array(
        '#title' => t('Concatenator'),
        '#type' => 'select',
        '#options' => array(',' => ', (AND)', '+' => '+ (OR)'),
        '#default_value' => $conf['concatenator'],
        '#prefix' => '<div class="clearfix">',
        '#suffix' => '</div>',
        '#description' => t("When the value from this context is passed on to a view as argument, the terms can be concatenated in the form of 1+2+3 (for OR) or 1,2,3 (for AND)."),
    );

    return $form;
}
